package com.example.student1.newlesson0112;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    CircleView diagram;
    float [] data = {124,1231,263,1241,342,234};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        diagram = (CircleView) findViewById(R.id.diagram);
        diagram.setData(data);
    }
}
