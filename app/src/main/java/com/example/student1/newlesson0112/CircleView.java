package com.example.student1.newlesson0112;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class CircleView extends View {

    private float [] dataInProcent;
    private float [] data;

    public CircleView(Context context) {
        super(context);
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);
        canvas.drawARGB(255, 255, 0, 255);


        /*
        Paint circlePaint = new Paint();
        circlePaint.setColor(Color.BLUE);
        circlePaint.setStyle(Paint.Style.STROKE);

        canvas.drawCircle(100,100,50,new Paint());

        Paint rectPaint = new Paint();
        rectPaint.setColor(Color.RED);
        rectPaint.setStyle(Paint.Style.STROKE);

        canvas.drawRect(200,100,300,150, new Paint());

        */

        /*
        Paint circlePaint = new Paint();
        RectF rect = new RectF(100,100,400,400);
        canvas.drawArc(rect,0,180,true,new Paint());
        */


        //Доделать круг и прямоугольники
        /*
        Paint circlePaint = new Paint();
        circlePaint.setColor(Color.YELLOW);
        //circlePaint.setStyle(Paint.Style.STROKE);
        float cX = getWidth() / 2;
        float cY = getHeight() / 4;
        float R = getHeight() / 4;
        canvas.drawCircle(cX, cY, R, circlePaint);


        int [] colors = {Color.RED,Color.GRAY,Color.GREEN,Color.BLACK,Color.BLUE};
        int RectCount = 20;
        Paint rectPaint = new Paint();
        for (int i = 0;i <= RectCount;i++){
            rectPaint.setColor(i + 1);
            //rectPaint.setStyle(Paint.Style.STROKE);
            canvas.drawRect(200,100,300,150, rectPaint);
        }

*/

    }

    public void setData(float[] array) {

        data = array;
        float sum = 0;
        for (float i : data) {
            sum += i;
        }
        float oneProcent = sum / 100;
        dataInProcent = new float[data.length];
        for (int i = 0;i < data.length;i++) {
            dataInProcent[i] = data[i] / oneProcent;
            Log.d("My log",i + " :" + dataInProcent[i]);
        }
    }
}
